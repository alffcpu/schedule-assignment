import { State, initialState } from "state/store";
import React, { createContext, useState } from "react";
import isFunction from "lodash/isFunction";
import isObject from "lodash/isObject";
import isUndefined from "lodash/isUndefined";

export const AppStateContext = createContext({ ...initialState });
const StateProvider: React.FC = ({ children }) => {
  const [state, setState] = useState({ ...initialState });

  const value: State = {
    ...state,
    setStateValue: (key, value) => {
      setState((state) => {
        if (isFunction(key)) {
          return key(state);
        }
        const changes = !isUndefined(value)
          ? { [`${key}`]: value }
          : isObject(key)
          ? { ...key }
          : {};
        return { ...state, ...changes };
      });
    },
  };
  return (
    <AppStateContext.Provider value={value}>
      {children}
    </AppStateContext.Provider>
  );
};

export default StateProvider;
