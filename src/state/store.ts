import { Maybe, ScheduleData } from "components/OpeningHours/types";
import sampleText from "sample";

export type State = {
  lang: "en" | "ru" | "fi";
  sampleText: string;
  schedule: Maybe<ScheduleData>;
  setStateValue: SetStateValueCallback;
  setData: LSCallback;
};

const emptyCallback = () => undefined;

export const initialState: State = {
  lang: "en",
  sampleText,
  schedule: JSON.parse(sampleText),
  setData: emptyCallback,
  setStateValue: emptyCallback,
};

type LSCallback = (newState: string[]) => void;
type SSVCallback = (state: State) => State;
export type SetStateValueCallback = (
  key: (keyof State & string) | Partial<State> | SSVCallback,
  value?: any
) => void;
