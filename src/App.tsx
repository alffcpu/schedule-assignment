import { Alert, Layout } from "antd";
import { ScheduleData } from "components/OpeningHours/types";
import AppContent from "components/AppContent";
import AppHeader from "components/AppHeader";
import AppLogo from "components/AppLogo";
import AppMenu from "components/AppMenu/AppMenu";
import JSONModal from "components/JSONModal/JSONModal";
import OpeningHours from "components/OpeningHours";
import React, { FC, useCallback, useState } from "react";
import useErrorBoundary from "use-error-boundary";
import useLang from "hooks/useLang";
import useStoreValue from "hooks/useStoreValue";

const App: FC = () => {
  const [modalOpened, setModalOpen] = useState(false);
  const [schedule] = useStoreValue<ScheduleData>("schedule");
  const [, setLang] = useLang();

  const handleMenuItemSelect = useCallback(
    (key: string) => {
      switch (key) {
        case "edit":
          setModalOpen((state) => !state);
          return;
        default:
          setLang(key);
      }
    },
    [setLang]
  );

  const { ErrorBoundary, didCatch, error } = useErrorBoundary();

  return (
    <Layout>
      <AppHeader>
        <AppLogo>O.Hours</AppLogo>
        <AppMenu handleSelect={handleMenuItemSelect} />
      </AppHeader>
      <AppContent>
        {didCatch ? (
          <Alert
            description={error.message}
            message="Runtime Error"
            showIcon={true}
            type="error"
          />
        ) : (
          <ErrorBoundary>
            {!!schedule && <OpeningHours schedule={schedule} />}
          </ErrorBoundary>
        )}
      </AppContent>
      <JSONModal active={modalOpened} onClose={() => setModalOpen(false)} />
    </Layout>
  );
};

export default App;
