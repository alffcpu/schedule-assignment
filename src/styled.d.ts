import "styled-components";
import {
  Colors,
  FontSizes,
  FontWeights,
  Gutters,
  LineHeights,
  Radius,
  Shadow,
  Spacing,
} from "config/theme";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: Colors;
    spacing: Spacing;
    gutters: Gutters;
    radius: Radius;
    shadow: Shadow;
    fontSizes: FontSizes;
    lineHeights: LineHeights;
    fontWeights: FontWeights;
  }
}
