export default {
  en: {
    translation: {
      "Opening Hours": "Opening Hours",
      monday: "Monday",
      tuesday: "Tuesday",
      wednesday: "Wednesday",
      thursday: "Thursday",
      friday: "Friday",
      saturday: "Saturday",
      sunday: "Sunday",
      Closed: "Closed",
      Today: "Today",
    },
  },
  ru: {
    translation: {
      "Opening Hours": "Часы работы",
      monday: "Понедельник",
      tuesday: "Вторник",
      wednesday: "Среда",
      thursday: "Четверг",
      friday: "Пятница",
      saturday: "Суббота",
      sunday: "Воскресенье",
      Closed: "Закрыто",
      Today: "Сегодня",
    },
  },
  fi: {
    translation: {
      "Opening Hours": "Aukioloajat",
      monday: "Maanantai",
      tuesday: "Tiistai",
      wednesday: "Keskiviikko",
      thursday: "Torstai",
      friday: "Perjantai",
      saturday: "Lauantai",
      sunday: "Sunnuntai",
      Closed: "Suljettu",
      Today: "Tänään",
    },
  },
};
