import { DefaultTheme } from "styled-components";

export type Colors = {
  black: string;
  white: string;
  primary: string;
  lightGray: string;
  neutralGray: string;
  darkGray: string;
  danger: string;
  neutralDanger: string;
};

const colors: Colors = {
  black: "#202125",
  white: "#ffffff",
  primary: "#5bcb02",
  lightGray: "#f8f8f8",
  neutralGray: "#eeeeee",
  darkGray: "#a1a2a4",
  danger: "#ff0000",
  neutralDanger: "#FFDCDC",
};

type SimpleSMLType<T> = {
  sm: T;
  md: T;
  lg: T;
};

type ExtendedSMLType<T> = SimpleSMLType<T> & {
  xl: T;
};

export type Gutters = ExtendedSMLType<number>;

const gutters: Gutters = {
  sm: 8,
  md: 16,
  lg: 32,
  xl: 48,
};

export type Spacing = ExtendedSMLType<string>;

const spacing = {
  sm: `${gutters.sm}px`,
  md: `${gutters.md}px`,
  lg: `${gutters.lg}px`,
  xl: `${gutters.xl}px`,
};

export type Radius = SimpleSMLType<string>;

const radius: Radius = {
  sm: "8px",
  md: "16px",
  lg: "32px",
};

export type Shadow = SimpleSMLType<string>;

const shadow: Shadow = {
  sm: "0 2px 4px 2px rgba(0,0,0,0.05)",
  md: "0 3px 6px 4px rgba(0,0,0,0.075)",
  lg: "0 4px 8px 6px rgba(0,0,0,0.1)",
};

export type FontSizes = SimpleSMLType<string>;

const fontSizes: FontSizes = {
  sm: "12px",
  md: "16px",
  lg: "24px",
};

export type LineHeights = SimpleSMLType<string>;

const lineHeights: LineHeights = {
  sm: "16px",
  md: "22px",
  lg: "30px",
};

export type FontWeights = {
  normal: number;
  medium: number;
  bold: number;
};

const fontWeights: FontWeights = {
  normal: 400,
  medium: 500,
  bold: 700,
};

const theme: DefaultTheme = {
  colors,
  gutters,
  spacing,
  radius,
  shadow,
  fontSizes,
  lineHeights,
  fontWeights,
};

export default theme;
