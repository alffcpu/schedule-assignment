import { Menu } from "antd";
import CheckIcon from "components/CheckIcon";
import MenuIcon from "components/AppMenu/MenuIcon";
import React, { FC } from "react";
import useStoreValue from "hooks/useStoreValue";

export type AppMenuProps = {
  handleSelect: (key: string) => void;
};

export const langs: Record<string, string> = {
  en: "English",
  ru: "Русский",
  fi: "Suomi",
};

const { Item, SubMenu } = Menu;

const AppMenu: FC<AppMenuProps> = ({ handleSelect }) => {
  const [lang] = useStoreValue("lang");
  return (
    <Menu
      mode="horizontal"
      overflowedIndicator={<MenuIcon />}
      selectable={false}
      theme="light"
    >
      <Item key="edit" onClick={() => handleSelect("edit")}>
        Edit JSON
      </Item>
      <SubMenu key={`${lang}`} title={`Language (${`${lang}`.toUpperCase()})`}>
        {Object.entries(langs).map(([code, title]) => (
          <Item key={code} onClick={() => handleSelect(`${code}`)}>
            {title}
            {lang === code && <CheckIcon />}
          </Item>
        ))}
      </SubMenu>
    </Menu>
  );
};

export default AppMenu;
