import { Menu } from "@styled-icons/entypo/Menu";
import styled, { css } from "styled-components";

const MenuIcon = styled(Menu)`
  ${({ theme: { fontSizes, colors } }) => css`
    width: ${fontSizes.lg};
    height: ${fontSizes.lg};
    color: ${colors.black};
  `}
`;

export default MenuIcon;
