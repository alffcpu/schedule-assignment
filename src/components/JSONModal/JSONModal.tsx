import { Form, Input, Modal, notification } from "antd";
import { FormInstance } from "antd/lib/form";
import { Maybe, ScheduleData } from "components/OpeningHours/types";
import { verifyScheduleData } from "components/JSONModal/utils";
import React, { FC, useCallback, useRef } from "react";
import isArray from "lodash/isArray";
import isObject from "lodash/isObject";
import useStoreValue from "hooks/useStoreValue";

const { TextArea } = Input;

export type JSONModalProps = {
  active: boolean;
  onClose: () => void;
};

type FormValues = {
  sampleText: string;
};

const JSONModal: FC<JSONModalProps> = ({ active, onClose }) => {
  const [sampleText, setSampleText] = useStoreValue("sampleText");
  const [, setSchedule] = useStoreValue("schedule");
  const formRef = useRef<Maybe<FormInstance>>(null);

  const handleUpdate = useCallback(async () => {
    try {
      const {
        sampleText,
      }: FormValues = await formRef.current?.validateFields();
      try {
        const scheduleData: ScheduleData = JSON.parse(sampleText);
        if (!isObject(scheduleData) || isArray(scheduleData)) throw new Error();

        const schedule = verifyScheduleData(scheduleData);
        setSampleText(JSON.stringify(schedule, null, 2));
        setSchedule(schedule);
        onClose();
      } catch (err) {
        notification.error({
          message: `JSON parse error`,
          description: "Please, enter valid JSON and it should be an object.",
          placement: "topRight",
        });
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error("err", err);
    }
  }, [onClose, setSampleText, setSchedule]);

  return (
    <Modal
      destroyOnClose
      okText="Update"
      onCancel={onClose}
      onOk={handleUpdate}
      title="Edit JSON"
      visible={active}
    >
      <Form<FormValues>
        ref={formRef}
        initialValues={{ sampleText }}
        name="edit_data"
      >
        <Form.Item
          name="sampleText"
          rules={[
            { required: true, message: "Please, enter valid JSON data sample" },
          ]}
        >
          <TextArea
            autoSize={{ minRows: 3, maxRows: 9 }}
            placeholder="Enter JSON data sample"
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default JSONModal;
