import {
  ScheduleData,
  ScheduleEntry,
  ScheduleEntryType,
  daysList,
} from "components/OpeningHours/types";
import isArray from "lodash/isArray";

export const verifyScheduleData = (scheduleData: ScheduleData): ScheduleData =>
  daysList.reduce<ScheduleData>((result, day) => {
    const entries: ScheduleEntry[] = [];
    if (isArray(scheduleData[day])) {
      scheduleData[day].forEach((entry) => {
        if (
          typeof entry?.type === "string" &&
          [ScheduleEntryType.CLOSE, ScheduleEntryType.OPEN].includes(
            entry.type
          ) &&
          typeof entry?.value === "number"
        ) {
          entries.push(entry);
        }
      });
    }
    return {
      ...result,
      [day]: entries,
      [day]: entries,
    };
  }, {} as ScheduleData);
