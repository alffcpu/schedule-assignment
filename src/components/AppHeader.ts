import { Layout } from "antd";
import styled, { css } from "styled-components";
import theme from "config/theme";

export const HEADER_HEIGHT = theme.gutters.lg * 2;

const AppHeader = styled(Layout.Header)`
  ${({ theme: { colors } }) => css`
    background-color: ${colors.white};
    height: ${HEADER_HEIGHT}px;
    display: flex;
    > * {
      background-color: ${colors.white};
    }
  `}
`;

export default AppHeader;
