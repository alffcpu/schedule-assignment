import { HEADER_HEIGHT } from "components/AppHeader";
import { Layout } from "antd";
import styled, { css } from "styled-components";

const AppLogo = styled(Layout.Header)`
  ${({ theme: { fontSizes, colors, fontWeights } }) => css`
    font-size: ${fontSizes.lg};
    height: ${HEADER_HEIGHT}px;
    line-height: ${HEADER_HEIGHT}px;
    color: ${colors.black};
    font-weight: ${fontWeights.medium};
  `}
`;

export default AppLogo;
