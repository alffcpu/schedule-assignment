import { HEADER_HEIGHT } from "components/AppHeader";
import { Layout } from "antd";
import styled, { css } from "styled-components";

const AppContent = styled(Layout.Content)`
  ${({ theme: { colors, gutters, spacing } }) => css`
    background-color: ${colors.lightGray};
    display: flex;
    width: 100%;
    height: 100%;
    padding: ${gutters.xl + gutters.md}px ${spacing.xl} ${spacing.xl}
      ${spacing.xl};
    align-items: center;
    justify-content: center;
    min-height: calc(100vh - ${HEADER_HEIGHT}px);
  `}
`;

export default AppContent;
