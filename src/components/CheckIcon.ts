import { Check2 } from "@styled-icons/bootstrap/Check2";
import styled, { css } from "styled-components";

const CheckIcon = styled(Check2)`
  ${({ theme: { fontSizes, spacing, colors } }) => css`
    width: ${fontSizes.md};
    height: ${fontSizes.md};
    margin-left: ${spacing.md};
    color: ${colors.primary};
  `}
`;

export default CheckIcon;
