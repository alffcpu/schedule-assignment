import { DayDescription } from "components/OpeningHours/useOpeningHoursData";
import { useTranslation } from "react-i18next";
import Cell from "components/OpeningHours/components/Cell";
import ClosedMark from "components/OpeningHours/components/ClosedMark";
import React, { FC } from "react";
import Row from "components/OpeningHours/components/Row";
import TimeRange from "components/OpeningHours/components/TimeRange";
import TodayMark from "components/OpeningHours/components/TodayMark";
import WeekDayName from "components/OpeningHours/components/WeekDayName";

type DayHoursProps = Omit<DayDescription, "key">;

const DayHours: FC<DayHoursProps> = ({ title, hours, isToday, isClosed }) => {
  const { t } = useTranslation();
  return (
    <Row>
      <Cell>
        <WeekDayName>{title}</WeekDayName>
        {isToday && <TodayMark>{t("Today")}</TodayMark>}
      </Cell>
      <Cell $justify="flex-end" $wrap="wrap">
        {isClosed ? (
          <ClosedMark>{t("Closed")}</ClosedMark>
        ) : (
          <>
            {hours.map((time) => (
              <TimeRange key={time}>{time}</TimeRange>
            ))}
          </>
        )}
      </Cell>
    </Row>
  );
};

export default DayHours;
