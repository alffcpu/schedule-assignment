import styled, { css } from "styled-components";

const TodayMark = styled.div`
  ${({ theme: { spacing, colors, fontSizes, fontWeights } }) => css`
    color: ${colors.primary};
    font-size: ${fontSizes.sm};
    text-transform: uppercase;
    font-weight: ${fontWeights.medium};
    margin-left: ${spacing.sm};
  `}
`;

export default TodayMark;
