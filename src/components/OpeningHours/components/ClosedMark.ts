import styled, { css } from "styled-components";

const ClosedMark = styled.div`
  ${({ theme: { colors } }) => css`
    color: ${colors.darkGray};
  `}
`;

export default ClosedMark;
