import styled, { css } from "styled-components";

type CellProps = {
  $display?: string;
  $justify?: string;
  $wrap?: string;
};

const Cell = styled.div<CellProps>`
  ${({ $display = "flex", $justify = "flex-start", $wrap = "nowrap" }) => css`
    display: ${$display};
    justify-content: ${$justify};
    flex-wrap: ${$wrap};
  `}
`;

export default Cell;
