import styled, { css } from "styled-components";

const HoursContainer = styled.div`
  ${({
    theme: { lineHeights, fontSizes, colors, spacing, fontWeights },
  }) => css`
    line-height: ${lineHeights.lg};
    font-size: ${fontSizes.lg};
    color: ${colors.black};
    border-bottom: 1px solid ${colors.darkGray};
    padding-bottom: ${spacing.sm};
    font-weight: ${fontWeights.bold};
    font-family: "Roboto", serif;
    width: 100%;
    > * {
      font-family: "Roboto", serif;
    }
  `}
`;

export default HoursContainer;
