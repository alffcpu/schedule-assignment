import styled, { css } from "styled-components";

const WeekDayName = styled.div`
  ${({ theme: { lineHeights, colors, fontSizes, fontWeights } }) => css`
    line-height: ${lineHeights.md};
    color: ${colors.black};
    font-size: ${fontSizes.md};
    font-weight: ${fontWeights.medium};
  `}
`;

export default WeekDayName;
