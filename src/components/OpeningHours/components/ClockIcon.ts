import { Clock } from "@styled-icons/bootstrap/Clock";
import styled, { css } from "styled-components";

const ClockIcon = styled(Clock)`
  ${({ theme: { fontSizes, colors, gutters, lineHeights } }) => css`
    width: ${fontSizes.lg};
    height: ${fontSizes.lg};
    color: ${colors.darkGray};
    margin-top: -${gutters.sm / 2}px;
    margin-right: ${gutters.md}px;
  `}
`;

export default ClockIcon;
