import styled, { css } from "styled-components";

const HoursContainer = styled.div`
  ${({ theme: { shadow, colors, radius, spacing } }) => css`
    background-color: ${colors.white};
    border-radius: ${radius.md};
    max-width: 480px;
    padding: ${spacing.lg} ${spacing.lg};
    box-shadow: ${shadow.md};
  `}
`;

export default HoursContainer;
