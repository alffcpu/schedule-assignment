import styled, { css } from "styled-components";

const Row = styled.div`
  ${({ theme: { lineHeights, spacing, colors } }) => css`
    line-height: ${lineHeights.md};
    padding: ${spacing.sm} 0;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    width: 100%;
    border-bottom: 1px solid ${colors.neutralGray};
    > *:first-child {
      padding-right: ${spacing.xl};
    }
  `}
`;

export default Row;
