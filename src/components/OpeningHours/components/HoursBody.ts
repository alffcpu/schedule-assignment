import styled from "styled-components";

const HoursBody = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export default HoursBody;
