import styled from "styled-components";

const TimeRange = styled.div`
  white-space: nowrap;
  display: inline-block;
  margin-left: 0.25em;
  &::after {
    content: ", ";
    display: inline-block;
  }
  &:last-child::after {
    content: "";
  }
`;

export default TimeRange;
