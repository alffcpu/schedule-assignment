import { ScheduleData } from "components/OpeningHours/types";
import { useTranslation } from "react-i18next";
import ClockIcon from "components/OpeningHours/components/ClockIcon";
import DayHours from "components/OpeningHours/components/DayHours";
import HoursBody from "components/OpeningHours/components/HoursBody";
import HoursContainer from "components/OpeningHours/components/HoursContainer";
import HoursHeading from "components/OpeningHours/components/HoursHeading";
import React, { FC } from "react";
import useOpeningHoursData from "components/OpeningHours/useOpeningHoursData";

export type OpeningHoursProps = {
  schedule: ScheduleData;
};

const OpeningHours: FC<OpeningHoursProps> = React.memo(({ schedule }) => {
  const { t } = useTranslation();
  const displayData = useOpeningHoursData(schedule);

  return (
    <HoursContainer>
      <HoursHeading>
        <ClockIcon />
        {t("Opening Hours")}
      </HoursHeading>
      <HoursBody>
        {displayData.map(({ key, ...props }) => (
          <DayHours key={key} {...props} />
        ))}
      </HoursBody>
    </HoursContainer>
  );
});

export default OpeningHours;
