import {
  ScheduleEntry,
  ScheduleEntryType,
} from "components/OpeningHours/types";

export const MAX_SECONDS = 86400;

export const getTimeFromSeconds = (sec: number) => {
  const seconds = sec >= MAX_SECONDS ? sec - MAX_SECONDS : sec;
  const hours = Math.floor(seconds / 3600);
  const minSeconds = seconds - hours * 3600;
  const minutes = Math.floor(minSeconds / 60);
  const displayHour = hours >= 12 ? hours - 12 : hours;
  return [
    `${displayHour || 12}`,
    minutes ? `${minutes}`.padStart(2, "0") : null,
    hours > 11 ? "PM" : "AM",
  ];
};

export type CombinedOpenClose = { [key in ScheduleEntryType]: number[] };

export const getOpenHours = (hours: ScheduleEntry[]): CombinedOpenClose => {
  return hours.reduce<CombinedOpenClose>(
    (result, { value, type }) => ({
      ...result,
      [type]: [...result[type], value],
    }),
    { [ScheduleEntryType.OPEN]: [], [ScheduleEntryType.CLOSE]: [] }
  );
};
