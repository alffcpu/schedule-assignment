import { ThemeProvider } from "styled-components";
import OpeningHours from "./OpeningHours";
import React, { FC } from "react";
import StateProvider from "state";
import renderer from "react-test-renderer";
import sample from "sample";
import theme from "config/theme";

const TestApp: FC = ({ children }) => (
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <StateProvider>{children}</StateProvider>
    </ThemeProvider>
  </React.StrictMode>
);

const schedule1 = JSON.parse(sample);
const empty: any = Object.keys(schedule1).reduce(
  (result, key) => ({
    ...result,
    [key]: [],
  }),
  {}
);

jest.mock("react-i18next", () => ({
  useTranslation: () => {
    return {
      t: (str: any) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe("OpeningHours component", () => {
  it("renders with most of edge cases", () => {
    const tree = renderer
      .create(
        <TestApp>
          <TestApp>
            <OpeningHours schedule={schedule1} />
          </TestApp>
        </TestApp>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders with all empty days", () => {
    const tree = renderer
      .create(
        <TestApp>
          <TestApp>
            <OpeningHours schedule={empty} />
          </TestApp>
        </TestApp>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
