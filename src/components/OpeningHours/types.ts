export type Maybe<T> = T | null;

export enum DayOfWeek {
  MONDAY = "monday",
  TUESDAY = "tuesday",
  WEDNESDAY = "wednesday",
  THURSDAY = "thursday",
  FRIDAY = "friday",
  SATURDAY = "saturday",
  SUNDAY = "sunday",
}

export const daysList: DayOfWeek[] = [
  DayOfWeek.MONDAY,
  DayOfWeek.TUESDAY,
  DayOfWeek.WEDNESDAY,
  DayOfWeek.THURSDAY,
  DayOfWeek.FRIDAY,
  DayOfWeek.SATURDAY,
  DayOfWeek.SUNDAY,
];

export enum ScheduleEntryType {
  OPEN = "open",
  CLOSE = "close",
}

export type ScheduleEntry = {
  type: ScheduleEntryType;
  value: number;
};

export type ScheduleData = {
  [key in DayOfWeek]: ScheduleEntry[];
};
