import {
  MAX_SECONDS,
  getOpenHours,
  getTimeFromSeconds,
} from "components/OpeningHours/utils";
import {
  ScheduleData,
  ScheduleEntryType,
  daysList,
} from "components/OpeningHours/types";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";
import merge from "lodash/merge";
import orderBy from "lodash/orderBy";

export type DayDescription = {
  title: string;
  hours: string[];
  isToday: boolean;
  isClosed: boolean;
  key: string;
};

type UseOpeningHoursData = (schedule: ScheduleData) => DayDescription[];

const useOpeningHoursData: UseOpeningHoursData = (schedule) => {
  const { t } = useTranslation();
  return useMemo(() => {
    const dayToday = new Date().getDay();
    const todayIndex = !dayToday ? 6 : dayToday - 1;
    const today = daysList[todayIndex];

    const orderedSchedule = merge({}, schedule);

    const weekReversed = [...daysList].reverse();
    weekReversed.forEach((dayOfWeek, index, allDays) => {
      const day = orderedSchedule[dayOfWeek];
      if (!day.length) return day;
      const orderedEntries = orderBy(day, "value", "asc");
      if (orderedEntries[0].type === ScheduleEntryType.CLOSE) {
        const prevDayOfWeek = allDays[index + 1]
          ? allDays[index + 1]
          : allDays[0];
        orderedSchedule[prevDayOfWeek].push({
          type: ScheduleEntryType.CLOSE,
          value: MAX_SECONDS + orderedEntries[0].value,
        });
        orderedSchedule[dayOfWeek] = day.slice(1);
      }
      return orderedEntries;
    });

    return Object.entries(orderedSchedule).reduce<DayDescription[]>(
      (result, [dayOfWeek, dayHours]) => {
        const { open, close } = getOpenHours(dayHours);
        const hours = open.reduce<string[]>((result, openSeconds, index) => {
          const closeSeconds = close[index];
          if (closeSeconds == null) return result;
          const [oHour, oMin, oMark] = getTimeFromSeconds(openSeconds);
          const [cHour, cMin, cMark] = getTimeFromSeconds(closeSeconds);
          return [
            ...result,
            `${oHour}${oMin ? `:${oMin}` : ""} ${oMark} - ${cHour}${
              cMin ? `:${cMin}` : ""
            } ${cMark}`,
          ];
        }, []);

        return [
          ...result,
          {
            key: dayOfWeek,
            title: t(dayOfWeek.toLowerCase()),
            isToday: today === dayOfWeek,
            isClosed: !hours.length,
            hours,
          },
        ];
      },
      []
    );
  }, [schedule, t]);
};

export default useOpeningHoursData;
