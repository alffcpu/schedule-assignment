import { AppStateContext } from "state";
import { State } from "state/store";
import { useContext, useMemo } from "react";

export type ValueOf<T> = T[keyof T];

export type StoreDispatch = (v: ValueOf<State>) => void;

const useStoreValue = <T = ValueOf<State>>(
  key: keyof State
): [T, StoreDispatch] => {
  const ctx = useContext(AppStateContext);
  const value: unknown = ctx[key];
  const dispatch = useMemo(() => ctx.setStateValue.bind(null, key), [
    ctx.setStateValue,
    key,
  ]);
  return [value as T, dispatch];
};

export default useStoreValue;
