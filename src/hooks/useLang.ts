import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import useStoreValue, { StoreDispatch } from "hooks/useStoreValue";

const useLang = (): [string, StoreDispatch] => {
  const [lang, setLang] = useStoreValue<string>("lang");
  const { i18n } = useTranslation();
  useEffect(() => {
    i18n.changeLanguage(`${lang}`);
  }, [i18n, lang]);
  return [lang, setLang];
};

export default useLang;
