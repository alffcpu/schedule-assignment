import { AppStateContext } from "state";
import { useContext } from "react";

const useStore = () => useContext(AppStateContext);

export default useStore;
