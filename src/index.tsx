import "assets/app.css";
import "assets/fonts.css";
import { ThemeProvider } from "styled-components";
import { initReactI18next } from "react-i18next";
import App from "App";
import React from "react";
import ReactDOM from "react-dom";
import StateProvider from "state";
import i18n from "i18next";
import resources from "config/i18";
import theme from "config/theme";

void i18n.use(initReactI18next).init({
  resources,
  lng: "en",
  keySeparator: false,
  fallbackLng: "en",
  interpolation: {
    escapeValue: false,
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <StateProvider>
        <App />
      </StateProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
