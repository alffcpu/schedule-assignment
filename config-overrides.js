const {
  override,
  enableEslintTypescript,
  disableEsLint,
  addBundleVisualizer,
  addWebpackPlugin,
} = require("customize-cra");
const CircularDependencyPlugin = require("circular-dependency-plugin");

module.exports = override(
  enableEslintTypescript(),
  disableEsLint(),

  addWebpackPlugin(
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /node_modules/,
      // add errors to webpack instead of warnings
      failOnError: true,
      // set the current working directory for displaying module paths
      cwd: process.cwd(),
    })
  ),
  addBundleVisualizer({}, true)
);
