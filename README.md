# Opening Hours Component #

**[LIVE DEMO](http://dev.thesuper.ru/ohours/)**

Assignment is [here](https://c.smartrecruiters.com/sr-company-attachments-prod-dc5/5f05b5736bbcbc0ff2f7e7f3/d299cdb5-c0de-4c9d-8573-94a5ffdee6cb?r=s3-eu-central-1)  

`npm start` - start dev server ([http://localhost:3000](http://localhost:3000))

`npm run test` - run test

`npm run format` - run eslint and prettier


###Build

1) `npm i` - install dependencies  
2) set `PUBLIC_URL` in **.env** file to appropriate value  
3) `npm run build` - build app to **/build/**  
